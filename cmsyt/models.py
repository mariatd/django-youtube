from django.db import models


class Videos(models.Model):
    title = models.CharField(max_length=200)
    url = models.CharField(max_length=200)
    selected = models.BooleanField(default=False)
    id = models.CharField(max_length=200, primary_key=True)

    def __str__(self):
        return self.title