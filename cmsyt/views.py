from django.shortcuts import render
from django.http import HttpResponse
from .models import Videos
import urllib.request
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from django.views.decorators.csrf import csrf_exempt
from urllib.error import URLError
from urllib.request import urlretrieve

class VideoContentHandler(ContentHandler):
    def __init__(self):
        self.inEntry = False
        self.inVideo = False
        self.title = ""
        self.url = ""
        self.id = ""
        self.buffer = []

    def startElement(self, name, attrs):
        if name == 'entry':
            self.inEntry = True
        elif self.inEntry:
            if name == 'title' or name == 'yt:videoId':
                self.inVideo = True
            elif name == 'link' and attrs.get('rel') == 'alternate':
                self.url = attrs.get('href')

    def endElement(self, name):
        if name == 'entry':
            self.inEntry = False
            if not Videos.objects.filter(url=self.url).exists():
                Videos.objects.create(title=self.title, url=self.url, id=self.id)
            self.buffer.append(f"    <li><a href='{self.url}'>{self.title}</a></li>\n")
            self.title = ""
            self.url = ""
            self.id = ""
        elif self.inEntry and self.inVideo:
            if name == 'title':
                self.title = ''.join(self.buffer)
            elif name == 'yt:videoId':
                self.id = ''.join(self.buffer)
            self.inVideo = False
            self.buffer = []

    def characters(self, chars):
        if self.inVideo:
            self.buffer.append(chars)


def download_xml():
    url = 'https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg'
    try:
        filename, _ = urlretrieve(url)
        parse_xml(filename)
    except URLError as e:
        print("Error al descargar el archivo XML:", e)


def parse_xml(filename):
    try:
        YoutubetParser = make_parser()
        YoutubeHandler = VideoContentHandler()
        YoutubetParser.setContentHandler(YoutubeHandler)
        YoutubetParser.parse(filename)
    except Exception as e:
        print("Error en el archivo XML:", e)


@csrf_exempt
def index(request):
    if request.method == 'GET':
        download_xml()

    elif request.method == 'POST':
        cuerpo = request.POST['selected']
        idVideo = request.POST['id']
        if cuerpo == 'true':
            c = Videos.objects.get(id=idVideo)
            c.selected = True
            c.save()
        else:
            c = Videos.objects.get(id=idVideo)
            c.selected = False
            c.save()
    non_selected_list = Videos.objects.filter(selected=False)
    selected_list = Videos.objects.filter(selected=True)
    return render(request, 'cmsyt/index.html', {'non_selected_list': non_selected_list, 'selected_list': selected_list})
